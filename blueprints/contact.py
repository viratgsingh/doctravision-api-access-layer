from flask import Blueprint,Flask, request
from flask_restful import Resource, Api, reqparse
import requests
from api_access_layer.modules.contact_team import contact_team
contact = Blueprint('contact', __name__)


        
class GetContact(Resource):
    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument('company', type=str, required=True)
        parser.add_argument('name', type=str, required=True)
        parser.add_argument('email', type=str, required=True)
        parser.add_argument('contact', type=str, required=True)
        parser.add_argument('message', type=str, required=True)
        parser.add_argument('messageType', type=str, required=True)
        args=parser.parse_args()
        
        content=request.get_json()
        result=contact_team(content=content)
        #result={"status_code":response.status_code}
        return result
