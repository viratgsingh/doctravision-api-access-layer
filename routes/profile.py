from api_access_layer.blueprints.profile import GetMediaAdminInfo,GetMediaAdminList,EditMediaAdmin,EditEndUser,GetEndUserInfo,GetEndUserList,GetClientInfo,GetClientUserList,GetClientList,EditProfilePage,VerifyProfilePage,GetNotifications, InviteProfile
def profile_routes(api):
    
    api.add_resource(EditProfilePage, '/api/access/publisher/profile/edit')
    api.add_resource(InviteProfile, '/api/access/publisher/profile/invite')
    api.add_resource(VerifyProfilePage, '/api/access/publisher/profile/verify')
    api.add_resource(GetNotifications, '/api/access/publisher/notifications')
    api.add_resource(GetClientList, '/api/access/publisher/list')
    api.add_resource(GetClientInfo, '/api/access/publisher/info')
    api.add_resource(GetClientUserList, '/api/access/publisher/users')
    api.add_resource(GetEndUserInfo, '/api/access/enduser/info')
    api.add_resource(GetEndUserList, '/api/access/enduser/list')
    api.add_resource(EditEndUser, '/api/access/enduser/edit')
    api.add_resource(GetMediaAdminInfo, '/api/access/mediaadmin/info')
    api.add_resource(GetMediaAdminList, '/api/access/mediaadmin/list')
    api.add_resource(EditMediaAdmin, '/api/access/mediaadmin/edit')