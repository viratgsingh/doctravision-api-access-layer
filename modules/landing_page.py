import requests
from datetime import datetime
from api_access_layer.error_handlers.errors import InternalServerError
def get_pagedata(media_type,email):
    
    data={"recent_media":None,"most_referred":[]}
    url="http://52.172.171.190:3001/core/video/list"

    
    try:
        #get user's publisher
        response=requests.get(url="http://52.172.171.190:3001/core/publisher/get",params={"email":email})
        result=response.json()
        if result["data"] is None:
            publisher=""
        else:
            publisher=result["data"]["publisher"]
        
        response=requests.get(url="http://52.172.171.190:3001/core/category/list")
        result=response.json()
        category_data=result["data"]
        category_l=[]
        for category_info in category_data:
            try:
                category=category_info["category"]
                category_l.append(category)
            except:
                continue
        
        #get most referred
        response=requests.get(url,params={"most_referred":"Yes","type":media_type,"publisher":publisher})
        most_ref_vids=response.json()
        most_ref_vids=most_ref_vids["data"]
        data["most_referred"]=most_ref_vids

        #get recent vids
        response=requests.get(url,params={"type":media_type})
        vids=response.json()
        vids=vids["data"][::-1]
        data["recent_media"]=vids

        #get category media
        for category in category_l:
            try:
                response=requests.get(url,params={"type":media_type,"category":category})
                category_media=response.json()
                data[category]=category_media["data"]
            except:
                continue

        return {"success":"true","message":"fetched page data successfully","data":data}
        
    
    except Exception as e:
      raise InternalServerError

def get_landingpage():
    
    data={"recent_media":{"image":[],"image360":[],"video":[],"video360":[]}}
    url="http://52.172.171.190:3001/core/video/list"

    try:
    
        response=requests.get(url="http://52.172.171.190:3001/core/category/list")
        result=response.json()
        category_data=result["data"]
        category_l=[]
        for category_info in category_data:
            try:
                category=category_info["category"]
                category_l.append(category)
            except:
                continue

        media_type_l=["image","image360","video","video360"]
        #get recent vids
        for media_type in media_type_l:
            response=requests.get(url,params={"type":media_type})
            vids=response.json()
            vids=vids["data"][::-1]
            data["recent_media"][media_type].append(vids)
        return {"success":"true","message":"fetched landing page data successfully","data":data}
            
   
    except Exception as e:
      raise InternalServerError

    
   
#print(get_pagedata(media_type="image360"))