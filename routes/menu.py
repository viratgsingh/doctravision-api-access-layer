from api_access_layer.blueprints.menu import ContactDoctra , FAQ , Notifications, Subscription, MenuDropdown
def menu_routes(api):
    api.add_resource(MenuDropdown, '/api/access/menu')
    api.add_resource(ContactDoctra, '/api/access/menu/contact')
    api.add_resource(FAQ, '/api/access/menu/faq')
    api.add_resource(Notifications, '/api/access/menu/notifications')
    api.add_resource(Subscription, '/api/access/menu/subscription')