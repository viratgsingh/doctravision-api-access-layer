from flask import Flask, request, redirect , send_file
from flask_restful import Api


from api_access_layer.blueprints.searchmedmedia import searchmedmedia
from api_access_layer.blueprints.login_details import logindetails
from api_access_layer.blueprints.infolists import infolists
from api_access_layer.blueprints.contact import contact
from api_access_layer.blueprints.profile import profile
from api_access_layer.blueprints.landing_page import landing_page
from api_access_layer.blueprints.image import image
from api_access_layer.blueprints.video import video
from api_access_layer.blueprints.upload import upload

from api_access_layer.routes.login import login_routes
from api_access_layer.routes.search import search_routes
from api_access_layer.routes.infolists import infolists_routes
from api_access_layer.routes.contact import contact_routes
from api_access_layer.routes.profile import profile_routes
from api_access_layer.routes.landing_page import landing_page_routes
from api_access_layer.routes.image import image_routes
from api_access_layer.routes.video import video_routes
from api_access_layer.routes.upload import upload_routes

from api_access_layer.error_handlers.errors import errors
from flask_cors import CORS, cross_origin
from flask_jwt_extended import JWTManager


#Initialize API
app = Flask(__name__)
#'/etc/letsencrypt/csr/0006_csr-certbot.pem','/etc/letsencrypt/csr/0006_key-certbot.pem'
#context=('/etc/letsencrypt/live/doctravision.arthism.com/fullchain.pem','/etc/letsencrypt/live/doctravision.arthism.com/privkey.pem')
#app.config['SERVER_NAME'] = 'doctravision.arthism.com:443'
app.config['SECRET_KEY'] = 'docsera'
app.config['JWT_SECRET_KEY'] = 'jwt-secret-string'
app.config['PROPAGATE_EXCEPTIONS'] = True

CORS(app)
jwt = JWTManager(app)
api = Api(app,errors=errors)

#Routes
search_routes(api)
login_routes(api)
infolists_routes(api)
contact_routes(api)
profile_routes(api)
landing_page_routes(api)
image_routes(api)
video_routes(api)
upload_routes(api)

#Blueprints
app.register_blueprint(searchmedmedia)
app.register_blueprint(logindetails)
app.register_blueprint(infolists)
app.register_blueprint(contact)
app.register_blueprint(profile)
app.register_blueprint(landing_page)
app.register_blueprint(image)
app.register_blueprint(video)
app.register_blueprint(upload)


#if __name__ == '__main__':
	#app.run(host='0.0.0.0',port=3010)
