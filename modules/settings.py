import requests
def get_settings():
    url="https://time.com/5952717/pandemic-dog/?utm_source=pocket-newtab-intl-en"
    try:
        result=requests.get(url=url)
        #result.json()
        return {"settings_page":"success"}
    except Exception as e:
        return {"error":str(e)}

def get_inviteuser():
    url="https://time.com/5952717/pandemic-dog/?utm_source=pocket-newtab-intl-en"
    try:
        result=requests.get(url=url)
        #result.json()
        return {"inviteuser_page":"success"}
    except Exception as e:
        return {"error":str(e)}

def get_edituser():
    url="https://time.com/5952717/pandemic-dog/?utm_source=pocket-newtab-intl-en"

    try:
        result=requests.get(url=url)
        #result.json()
        return {"edituser_page":"success"}
    except Exception as e:
        return {"error":str(e)}


def invite_user(url,content):
    try:
        details={
                    "full_name":content["full_name"],
                    "email":content["email"],
                    "degrees":content["degrees"],
                    "current_employment":content["current_employment"]
                }
        response=requests.post(url,json=details)
        #return response.json()
        dummy=details
        dummy["invite_user"]="success"
        return dummy
    except Exception as e:
        return {"error":str(e)}

def invite_user_again(url,content):
    try:
        details={
                    "full_name":content["full_name"],
                    "email":content["email"],
                    "degrees":content["degrees"],
                    "current_employment":content["current_employment"]
                }
        response=requests.post(url,params=details)
        #return response.json()
        dummy=details
        dummy["invite_user_again"]="success"
        return dummy
    except Exception as e:
        return {"error":str(e)}

def edit_user(url,content,request):
    try:
        upload = request.files.getlist("profile_pic")[0]
        file_link = "http://s3_success/"+upload.filename
        content_type = upload.content_type.split('/')[0]
        if content_type!="image":
            file_link="not an image"
        else:
            pass
        
    except:
        file_link="no image uploaded"
    
    try:
        details={   
                    "file_link":file_link,
                    "full_name":content["full_name"],
                    "email":content["email"],
                    "degrees":content["degrees"],
                    "current_employment":content["current_employment"]
                }
        response=requests.post(url,json=details,files={"profile_pic":upload})
        #return response.json()
        dummy=details
        dummy["invite_user"]="success"
        return dummy
    except Exception as e:
        return {"error":str(e)}



def get_notifications_settings():
    url="https://time.com/5952717/pandemic-dog/?utm_source=pocket-newtab-intl-en"
    try:
        result=requests.get(url=url)
        #result.json()
        return {"notifications_settings_page":"success"}
    except Exception as e:
        return {"error":str(e)}

def get_account_settings():
    url="https://time.com/5952717/pandemic-dog/?utm_source=pocket-newtab-intl-en"
    try:
        result=requests.get(url=url)
        #result.json()
        return {"account_settings_page":"success"}
    except Exception as e:
        return {"error":str(e)}

