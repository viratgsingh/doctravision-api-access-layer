from flask import Blueprint,Flask, request
from flask_restful import Resource, Api, reqparse
import requests
from api_access_layer.modules.menu import get_contactdoctra,get_faq,get_menu,get_notifications,get_subscription

menu = Blueprint('menu', __name__)

class ContactDoctra(Resource):
    def get(self):
       
        result=get_contactdoctra()
        
        #result={"status_code":response.status_code}
        return result

class FAQ(Resource):
    def get(self):
       
    
        result=get_faq()
        
        #result={"status_code":response.status_code}
        return result

class MenuDropdown(Resource):
    def get(self):
        
    
        result=get_menu()
        
        #result={"status_code":response.status_code}
        return result

class Notifications(Resource):
    def get(self):
       
    
        result=get_notifications()
        
        #result={"status_code":response.status_code}
        return result

class Subscription(Resource):
    def get(self):
        
    
        result=get_subscription()
        
        #result={"status_code":response.status_code}
        return result
