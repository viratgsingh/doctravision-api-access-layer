from api_access_layer.blueprints.saved_media import GetSavedMedia
def savedmedia_routes(api):
    api.add_resource(GetSavedMedia, '/api/access/savedmedia')