from flask import Blueprint,Flask, request
from flask_restful import Resource, Api, reqparse
import requests
from api_access_layer.modules.image_details import capture_frame, edit_frame

image = Blueprint('image', __name__)

class CaptureFrame(Resource):
    def get(self):
        parser = reqparse.RequestParser()
        parser.add_argument('timestamp', type=str, required=True)
        parser.add_argument('link', type=str, required=True)
        args=parser.parse_args()
        args["media_type"]="video"
        result=capture_frame(params=args)
        
        #result={"status_code":response.status_code}
        return result

class CaptureFrame360(Resource):
    def get(self):
        parser = reqparse.RequestParser()
        parser.add_argument('timestamp', type=str, required=True)
        parser.add_argument('link', type=str, required=True)
        args=parser.parse_args()
        args["media_type"]="video360"
        result=capture_frame(params=args)
        
        #result={"status_code":response.status_code}
        return result

class EditFrame(Resource):
    def get(self):
        parser = reqparse.RequestParser()
        parser.add_argument('frame', type=str, required=True)
        args=parser.parse_args()
        input_file=request.files.getlist("file")[0]
        result=edit_frame(input_file=input_file,frame=args["frame"])
        
        #result={"status_code":response.status_code}
        return result