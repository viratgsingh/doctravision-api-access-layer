from flask import Blueprint,Flask, request
from flask_restful import Resource, Api, reqparse
import requests
from api_access_layer.modules.saved_media import  get_savedmedia_type

saved_media = Blueprint('saved_media', __name__)

class GetSavedMedia(Resource):
    def get(self):
        parser = reqparse.RequestParser()
        parser.add_argument('url', type=str, required=True)
        parser.add_argument('type', type=str, required=False)
        args=parser.parse_args()
        url=args["url"]
        args.pop("url",None)
    
        result=get_savedmedia_type(url=url , params=args)
        
        #result={"status_code":response.status_code}
        return result

    