import requests
from api_access_layer.error_handlers.errors import InternalServerError
import random
import os

def upload_media(input_file, media_type):
    try:
        #upload media
        frmt=input_file.filename.split('.')[1]
        input_folder="Uploads"
        upload_name="upload_"+str(random.randint(10000000,99999999))+"."+frmt
        s3_layer="http://52.172.171.190:3501/vendor/file/uploadFile"
        data={
                "file":(upload_name,input_file),"folder":(None,input_folder)
        }
        upload_media=requests.post(url=s3_layer,files=data)
        upload_media= upload_media.json()
        upload_link=upload_media["data"]["Location"]

        #upload preview image (if video or video360)
        if ((media_type=="video") | (media_type=="video360")):
            image_name=upload_name.replace(frmt,"jpeg")
            command="ffmpeg -i '{}' -ss 00:00:5 -vframes 1 -f image2 '{}'".format(upload_link,image_name)
            os.system(command)
            image_file=open(image_name,"rb")
            data={
                "file":(image_name,image_file),"folder":(None,input_folder)
            }
            upload_media=requests.post(url=s3_layer,files=data)
            upload_media= upload_media.json()
            preview_link=upload_media["data"]["Location"]
            os.remove(image_name)
        else:
            preview_link=None

        return {"success":"true","message":"uploaded media successfully","data":{"link":upload_link,"type":media_type,"preview_link":preview_link}}
    except:
        raise InternalServerError

def upload_media_view(params):
    try:
        #check
        video_core_get="http://52.172.171.190:3001/core/video/get"
    
        check_video=requests.get(url=video_core_get,params=params)
        check_video=check_video.json()
        return {"success":"true","message":"succesffully retrieved media info","data":check_video["data"]}
       

    except:
        raise InternalServerError

def upload_media_info(content):
    try:
        #check
        video_core_get="http://52.172.171.190:3001/core/video/get"
    
        check_video=requests.get(url=video_core_get,params={"name":content["name"],"type":content["type"]})
        check_video=check_video.json()
        if "data" in check_video:
            if check_video["data"] is None:
                pass
            else:
                return {"success":"false","message":"media already exists","data":{}}

        #create
        video_core_create="http://52.172.171.190:3001/core/video/create"
        save_media=requests.post(url=video_core_create,data=content)
        save_media=save_media.json()
        
        return {"success":"true","message":"uploaded media info successfully","data":save_media["data"]}
    except:
        raise InternalServerError

def upload_media_taginfo(content):
    try:
        query={"name":content["name"],"type":content["type"]}
        if "description" in content:
            pass
        else:
            content["description"]=None
        #get added tags list
        video_core_create="http://52.172.171.190:3001/core/video/get"
        get_media=requests.get(url=video_core_create,params=query)
        get_media=get_media.json()
        if "added_tags" in get_media["data"]:

            tags_l=get_media["data"]["added_tags"]
            tags_l.append(content["tag"]+": "+content["start"]+"-"+content["end"])
            data={
                content["tag"]+"_description":content["description"],
                "added_tags":tags_l
                } 
        else:
            data={
                content["tag"]+"_description":content["description"],
                "added_tags":[content["tag"]+": "+content["start"]+"-"+content["end"]]
                } 


        #update media
        video_core_create="http://52.172.171.190:3001/core/video/updateOne"
        save_media=requests.post(url=video_core_create,params=query,json=data)
        save_media=save_media.json()
        
        return {"success":"true","message":"uploaded media info successfully","data":save_media["data"]}
    except:
        raise InternalServerError