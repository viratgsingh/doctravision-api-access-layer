from flask import Blueprint,Flask, request
from flask_restful import Resource, Api, reqparse
from flask_jwt_extended import jwt_required, get_jwt_identity
import requests
from api_access_layer.modules.search import search_media,filter_results, requestmedia, search_recom

searchmedmedia = Blueprint('searchmedmedia', __name__)

class SearchRecommendations(Resource):
    @jwt_required()
    def get(self,text):
        email=get_jwt_identity()
        result=search_recom(text=text,email=email)
        return result

class Search(Resource):
    @jwt_required()
    def get(self):
        parser = reqparse.RequestParser()
        email=get_jwt_identity()
        parser.add_argument('name', type=str, required=True)
        args=parser.parse_args()
    
        result=search_media( params=args)
        
        #result={"status_code":response.status_code}
        return result


class FilterResults(Resource):
    @jwt_required()
    def get(self):
        parser = reqparse.RequestParser()
        parser.add_argument('name', type=str, required=True)
        parser.add_argument('duration', type=str, required=False)
        parser.add_argument('sortBy', type=str, required=False)
        parser.add_argument('tags', type=str, required=False)
        
        email=get_jwt_identity()
        args=parser.parse_args()
    
        result=filter_results( params=args)
        
        #result={"status_code":response.status_code}
        return result


class RequestMedia(Resource):
    @jwt_required()
    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument('search_string', type=str, required=True)
        parser.add_argument('specifics', type=str, required=False)
        parser.add_argument('purpose', type=str, required=False)
        parser.add_argument('require', type=str, required=True)
        args=parser.parse_args()
        args["email"]=get_jwt_identity()
        result=requestmedia(params=args)
        
        #result={"status_code":response.status_code}
        return result



