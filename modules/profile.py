import requests
import random
from datetime import datetime
from api_access_layer.error_handlers.errors import InternalServerError
from api_access_layer.modules.enc_dec import encrypt_password,decrypt_password
from flask_jwt_extended import create_access_token, create_refresh_token, jwt_required, get_jwt_identity
def editprofile(params,content):
    
        
    params={"email":params["profile_email"]}

   
    old_email=params["email"]
    if "email" in content:
        new_email=content["email"]
    else:
        new_email=old_email
    
    if old_email!=new_email:
        verification_code=str(random.randint(1000000,9999999))
        content["verification_code"]=verification_code
        content["verified"]="false"
        # vendor_email="http://52.172.171.190:3501/vendor/email/send"
        # message="Your verification code is "+verification_code
        # email_body={    
        #             "email": new_email, 
        #             "subject": "Doctravision: Email Verification", 
        #             "body": message
        #             }
        # vendor_email_output=requests.post(url=vendor_email,data=email_body)
        # vendor_email_output=vendor_email_output.json()
        access_token = create_access_token(identity = new_email)
        refresh_token = create_refresh_token(identity = new_email)
        url="http://52.172.171.190:3001/core/publisher/updateOne"
        
        response=requests.post(url,params=params,json=content)
        response=response.json()
        return {"success":"true","message":"edited profile successfully","data":{"email":response["data"]["email"],"access_token":access_token,"refresh_token":refresh_token,"verification_code":content["verification_code"]}}
    else:
        pass


    url="http://52.172.171.190:3001/core/publisher/updateOne"
    response=requests.post(url,params=params,json=content)
    response=response.json()
    if response["data"] != None:
        response["data"].pop("encrypted_password",None)
        response["data"].pop("encryption_key",None)
        response["data"].pop("verification_code",None)
    return {"success":"true","message":"edited profile successfully","data":response["data"]}

    


def invite_user(content):
    try:
         #check limit
        count_users="http://52.172.171.190:3001/core/publisher/count"
        query={"invited_by":content["invited_by"]}
        user_count=requests.get(url=count_users,params=query)
        user_count=user_count.json()
        user_count=user_count["data"]

        #check plan
        client_info="http://52.172.171.190:3001/core/publisher/get"
        query={"email":content["invited_by"]}
        plan=requests.get(url=client_info,params=query)
        plan=plan.json()
        if "data" in plan:
            if plan["data"] is None:
                return {"success":"fail","message":"invalid admin","data":{}}
            else:
                plan=plan["data"]["plan"]
        if (plan=="Deluxe Unlimited") & (user_count>=10):
            return {"success":"fail","message":"max user limit reached","data":{}}
        elif (plan=="Ultimate Publisher") & (user_count>=1000):
            return {"success":"fail","message":"max user limit reached","data":{}}
        else:
            pass



        url="http://52.172.171.190:3001/core/publisher/create"
        if "degress" in content:
            pass
        else:
            content["degrees"]=""

        if "current_employment" in content:
            pass
        else:
            content["current_employment"]=""

        content["status"]="Invited on "+str(datetime.now().date())
        response=requests.post(url,json=content)
        response=response.json()
        return {"success":"true","message":"invited profile successfully","data":{"email":response["data"]["email"]}}
    
    except Exception as e:
        raise InternalServerError

def get_client_list():
    try:
        url="http://52.172.171.190:3001/core/publisher/list"
        response=requests.get(url=url,params={"client":"Yes"})
        response=response.json()
        client_l=[]
        if response["data"]!=None:
            for client in response["data"]:
                client.pop("encrypted_password",None)
                client.pop("encryption_key",None)
                client.pop("verification_code",None)
                client_l.append(client)
        
        
        return {"success":"true","message":"fetched client list succcessfully","data":client_l}
    except Exception as e:
        raise InternalServerError

def get_client_info(params):
    try:
        url="http://52.172.171.190:3001/core/publisher/get"
        response=requests.get(url=url,params=params)
        response=response.json()
        if response["data"]!= None:
            response["data"].pop("encrypted_password",None)
            response["data"].pop("encryption_key",None)
        return {"success":"true","message":"fetched client info succcessfully","data":response["data"]}
    except Exception as e:
        raise InternalServerError

def get_clientuser_list(params):
    try:
        url="http://52.172.171.190:3001/core/publisher/list"
        response=requests.get(url=url,params=params)
        response=response.json()
        user_list=response["data"]
        admin_get="http://52.172.171.190:3001/core/publisher/get"
        response1=requests.get(url=admin_get,params={"email":params["invited_by"]})
        response1=response1.json()
        admin_info=response1["data"]
        user_list.append(admin_info)
		      
        
        return {"success":"true","message":"fetched client user list succcessfully","data":user_list[::-1]}
    except Exception as e:
        raise InternalServerError
def get_enduser_info(params):
    try:
        url="http://52.172.171.190:3001/core/publisher/get"
        response=requests.get(url=url,params=params)
        response=response.json()
        if response["data"]!= None:
            response["data"].pop("encrypted_password",None)
            response["data"].pop("encryption_key",None)
        return {"success":"true","message":"fetched enduser info succcessfully","data":response["data"]}
    except Exception as e:
        raise InternalServerError

def get_enduser_list():
    try:
        url="http://52.172.171.190:3001/core/publisher/list"
        response=requests.get(url=url,params={"admin":"No"})
        response=response.json()
        user_l=[]
        if response["data"]!=None:
            for user in response["data"]:
                user.pop("encrypted_password",None)
                user.pop("encryption_key",None)
                user.pop("verification_code",None)
                user_l.append(user)
        return {"success":"true","message":"fetched enduser list succcessfully","data":user_l}
    except Exception as e:
        raise InternalServerError


def get_mediaadmin_info(params):
    try:
        url="http://52.172.171.190:3001/core/publisher/get"
        response=requests.get(url=url,params=params)
        response=response.json()
        if response["data"]!= None:
            response["data"].pop("encrypted_password",None)
            response["data"].pop("encryption_key",None)
        return {"success":"true","message":"fetched media admin info succcessfully","data":response["data"]}
    except Exception as e:
        raise InternalServerError

def get_mediaadmin_list():
    try:
        url="http://52.172.171.190:3001/core/publisher/list"
        response=requests.get(url=url,params={"admin":"Yes"})
        response=response.json()
        return {"success":"true","message":"fetched media admin list succcessfully","data":response["data"]}
    except Exception as e:
        raise InternalServerError


def verifyemail(content):
    url="http://52.172.171.190:3001/core/publisher/get"
    
    content["verified"]="false"
    
    try:
        check_code=requests.get(url,params=content)
        check_code=check_code.json()
        if "data" in check_code is None:
           
            return{"success":"true","message":"checked verification code successfully","data":None}
        else:
            url2="http://52.172.171.190:3001/core/publisher/updateOne"
            requests.post(url=url2,params=content,json={"verified":"true"})
            return{"success":"true","message":"checked verification code successfully","data":check_code["data"]["email"]}
       
    except Exception as e:
        return {"error":str(e)}

def get_notifications(params):
    url="http://52.172.171.190:3001/core/notification/list"
    

    
    try:
        data=[]
        notification_data=requests.get(url,params=params)
        notification_data=notification_data.json()
        notifications=notification_data["data"][::-1]
        # rec=datetime.now()
        # rec_notifications={}
        # for notification in notifications:
        #     date=notification["created_at"]
        #     date=datetime.strptime(date,"%d/%m/%y %H:%M:%S")
        #     diff=rec-date
        #     diff=diff.days+diff.seconds/3600
        #     rec_notifications[diff]=notification
        # notification=list(rec_notifications.items())
        # notification.sort()
        # for i in notification:
        #     data.append(i[1])

        return {"success":"true","message":"fetched notfication data successfully","data":notifications}
    except Exception as e:
        raise InternalServerError

    








