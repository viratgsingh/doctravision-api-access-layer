from api_access_layer.blueprints.contact import GetContact
def contact_routes(api):
    api.add_resource(GetContact, '/api/access/contact/get')