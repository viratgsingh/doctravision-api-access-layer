from api_access_layer.blueprints.upload import *

def upload_routes(api):
    api.add_resource(UploadVideo, '/api/access/creator/upload/video')
    api.add_resource(UploadVideoInfo, '/api/access/creator/upload/video/info')
    api.add_resource(UploadVideoTagInfo, '/api/access/creator/upload/video/tag')
    api.add_resource(UploadVideoView, '/api/access/publisher/video/view')

    api.add_resource(UploadVideo360, '/api/access/creator/upload/video360')
    api.add_resource(UploadVideo360Info, '/api/access/creator/upload/video360/info')
    api.add_resource(UploadVideo360TagInfo, '/api/access/creator/upload/video360/tag')
    api.add_resource(UploadVideo360View, '/api/access/publisher/video360/view')

    api.add_resource(UploadImage, '/api/access/creator/upload/image')
    api.add_resource(UploadImageInfo, '/api/access/creator/upload/image/info')
    api.add_resource(UploadImageTagInfo, '/api/access/creator/upload/image/tag')
    api.add_resource(UploadImageView, '/api/access/publisher/image/view')

    api.add_resource(UploadImage360, '/api/access/creator/upload/image360')
    api.add_resource(UploadImage360Info, '/api/access/creator/upload/image360/info')
    api.add_resource(UploadImage360TagInfo, '/api/access/creator/upload/image360/tag')
    api.add_resource(UploadImage360View, '/api/access/publisher/image360/view')

    api.add_resource(RegisterTag, '/api/access/creator/register/tag')
    api.add_resource(EditTag, '/api/access/creator/edit/tag')
    api.add_resource(RegisterCategory, '/api/access/creator/register/category')
    api.add_resource(EditCategory, '/api/access/creator/edit/category')
    api.add_resource(DeleteCategory, '/api/access/creator/delete/category')