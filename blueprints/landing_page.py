from flask import Blueprint,Flask, request
from flask_restful import Resource, Api, reqparse
from flask_jwt_extended import create_access_token, create_refresh_token, jwt_required, get_jwt_identity
import requests
from api_access_layer.modules.landing_page import get_pagedata, get_landingpage

landing_page = Blueprint('landing_page', __name__)

class LandingPage(Resource):
    def get(self):
        
        result=get_landingpage()
        
        return result

class LandingPageVideo(Resource):
    @jwt_required()
    def get(self):
        media_type="video"
        email=get_jwt_identity()
        result=get_pagedata(media_type=media_type,email=email)
        
        return result

class LandingPageVideo360(Resource):
    @jwt_required()
    def get(self):
        media_type="video360"
        email=get_jwt_identity()
        result=get_pagedata(media_type=media_type,email=email)
        
        return result

class LandingPageImage(Resource):
    @jwt_required()
    def get(self):
        media_type="image"
        email=get_jwt_identity()
        result=get_pagedata(media_type=media_type,email=email)
        
        return result

class LandingPageImage360(Resource):
    @jwt_required()
    def get(self):
        media_type="image360"
        email=get_jwt_identity()
        result=get_pagedata(media_type=media_type,email=email)
        
        return result