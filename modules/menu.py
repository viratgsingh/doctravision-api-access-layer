import requests
def get_menu():
    url="https://time.com/5952717/pandemic-dog/?utm_source=pocket-newtab-intl-en"
    try:
        result=requests.get(url=url)
        #result.json()
        return {"menu_dropdown":"success"}
    except Exception as e:
        return {"error":str(e)}

def get_notifications():
    url="https://time.com/5952717/pandemic-dog/?utm_source=pocket-newtab-intl-en"
    try:
        result=requests.get(url=url)
        #result.json()
        return {"notifications_page":"success"}
    except Exception as e:
        return {"error":str(e)}

def get_subscription():
    url="https://time.com/5952717/pandemic-dog/?utm_source=pocket-newtab-intl-en"
    try:
        result=requests.get(url=url)
        #result.json()
        return {"subscription_page":"success"}
    except Exception as e:
        return {"error":str(e)}

def get_faq():
    url="https://time.com/5952717/pandemic-dog/?utm_source=pocket-newtab-intl-en"
    try:
        result=requests.get(url=url)
        #result.json()
        return {"faq_page":"success"}
    except Exception as e:
        return {"error":str(e)}

def get_contactdoctra():
    url="https://time.com/5952717/pandemic-dog/?utm_source=pocket-newtab-intl-en"
    try:
        result=requests.get(url=url)
        #result.json()
        return {"contact_page":"success"}
    except Exception as e:
        return {"error":str(e)}
