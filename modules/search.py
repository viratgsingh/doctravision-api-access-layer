import requests
from datetime import datetime
from api_access_layer.error_handlers.errors import InternalServerError
def filter_search(params):
    if "most_referred" in params:
        if params["most_referred"]==1:
            params["most_referred"]="Yes"
        else: 
            params["most_referred"]="No"

    

    data={"data":[]}
    url="http://52.172.171.190:3001/core/video/list"

    
    
    response=requests.get(url="http://52.172.171.190:3001/core/category/list")
    result=response.json()
    category_data=result["data"]
    category_l=[]
    for category_info in category_data:
        try:
            category=category_info["category"]
            category_l.append(category)
        except:
            continue


    
    try:
        
        

        if "recently_added" in params:
            if params["recently_added"]==1:
                params.pop("recently_added",None)
                #get recent vids
                response=requests.get(url,params=params)
                vids=response.json()
                vids=vids["data"][::-1]
                data["data"]=vids
            else:
                params.pop("recently_added",None)
                response=requests.get(url,params=params)
                most_ref_vids=response.json()
                most_ref_vids=most_ref_vids["data"]
                data["data"]=most_ref_vids
        else:
            #get most referred
            response=requests.get(url,params=params)
            most_ref_vids=response.json()
            most_ref_vids=most_ref_vids["data"]
            data["data"]=most_ref_vids

        #get category media
        # for category in category_l:
        #     try:
        #         response=requests.get(url,params={"most_referred":"No","type":media_type,"category":category})
        #         category_media=response.json()
        #         data[category]=category_media["data"]
        #     except:
        #         continue

        return {"success":"true","message":"fetched page data successfully","data":data}
        
    
    except Exception as e:
      raise InternalServerError

def requestmedia(params):
    
    try:
        url="http://52.172.171.190:3001/core/video/create"
        response=requests.post(url=url, json=params)
        result=response.json()
        return {"success":"true","message":"fetched request media data successfully","data":result["data"]}
        
    
    except Exception as e:
      raise InternalServerError

def search_media(params):
    try:
        search_data={"search_results":[],"search_tags":[]}
        url="http://52.172.171.190:3001/core/video/list"
        response=requests.get(url=url, params=params)
        result=response.json()
        vids=result["data"]
        search_data["search_results"]=vids
        tags_l=[]
        for vid in vids:
            try:
                added_tags=vid["added_tags"]
                for added_tag in added_tags:

                    tag=added_tag.split(':')
                    if type(tag)==list:
                        tag=tag[0]
                        tags_l.append(tag)
                    else:
                        continue
            except:
                continue
        tags_l=list(set(tags_l))
        search_data["search_tags"]=tags_l
        return {"success":"true","message":"fetched media data successfully","data":search_data}
        
    
    except Exception as e:
      raise InternalServerError

def search_recom(text,email):
    try:
        #get user's publisher
        response=requests.get(url="http://52.172.171.190:3001/core/publisher/get",params={"email":email})
        result=response.json()
        if result["data"] is None:
            publisher=None
        else:
            publisher=result["data"]["publisher"]

        #query for search recommendations
        query={
                "publisher":publisher,
                "name":text
            }
        
        media_recom=[]
        url="http://52.172.171.190:3001/core/video/search"
        response=requests.get(url=url,params=query)
        result=response.json()
        vids=result["data"]
        for vid in vids:
            media_recom.append(vid["name"])
        return {"success":"true","message":"fetched media recommendations successfully","data":media_recom}
    except Exception as e:
      raise InternalServerError


def filter_results(params):
    query={}
    query["name"]=params["name"]
    if params["duration"]=="full":
        pass
    elif params["duration"]=="extracts":
        query["type"]="extract"
    else:
        pass

    if params["sortBy"]=="most_referred":
        query["most_referred"]="Yes"
    else:
        pass

    result={"success":"true","message":"filtered result successfully","data":[]}
    if params["sortBy"]=="newest":
        if "tags" in params:
            if params["tags"] is None:
                url="http://52.172.171.190:3001/core/video/list"
                response=requests.get(url,params=query)
                vids=response.json()
                vids=vids["data"][::-1]
                result["data"]=vids
            else:
                query["tags"]=params["tags"]
                url="http://52.172.171.190:3001/core/video/compile"
                response=requests.get(url,params=query)
                vids=response.json()
                vids=vids["data"][::-1]
                result["data"]=vids

                    
        else:
            url="http://52.172.171.190:3001/core/video/list"
            response=requests.get(url,params=query)
            vids=response.json()
            vids=vids["data"][::-1]
            result["data"]=vids
    else:
        if "tags" in params:
            if params["tags"] is None:
                url="http://52.172.171.190:3001/core/video/list"
                response=requests.get(url,params=query)
                vids=response.json()
                vids=vids["data"]
                result["data"]=vids
            else:
                query["tags"]=params["tags"]
                url="http://52.172.171.190:3001/core/video/compile"
                response=requests.get(url,params=query)
                vids=response.json()
                vids=vids["data"]
                result["data"]=vids

                    
        else:
            url="http://52.172.171.190:3001/core/video/list"
            response=requests.get(url,params=query)
            vids=response.json()
            vids=vids["data"]
            result["data"]=vids

    #to remove media request from results
    mediareqs=[]
    for vid in result["data"]:
        if "link" in vid:
            continue
        else:
            mediareqs.append(vid)
    vids=result["data"]
    result["data"]=[vid for vid in vids if vid not in mediareqs]


    return result



            

        

    


       
    
    
