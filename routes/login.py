
#from blueprints.login_details import GetEmail , GetPassword ,GetLoginPage, ValidateLogin
from api_access_layer.blueprints.login_details import CreateLogin, ValidateLogin, AddNewClient,TokenRefresh
def login_routes(api):
    api.add_resource(ValidateLogin, '/api/access/publisher/login/validate')
    api.add_resource(CreateLogin, '/api/access/publisher/login/create')
    api.add_resource(AddNewClient, '/api/access/publisher/login/add')
    api.add_resource(TokenRefresh, '/api/access/publisher/token/refresh')
    # api.add_resource(GetPassword, '/api/access/login/get/password')
    # api.add_resource(GetLoginPage, '/api/access/login')

