import requests
from api_access_layer.error_handlers.errors import InternalServerError

def register_tag(content):
    try:
        #check
        tag_core_get="http://52.172.171.190:3001/core/tag/get"
    
        check_tag=requests.get(url=tag_core_get,params={"tag":content["tag"],"category":content["category"]})
        check_tag=check_tag.json()
        if "data" in check_tag:
            if check_tag["data"] is None:
                pass
            else:
                return {"success":"false","message":"tag already exists","data":{}}

        #create
        tag_create="http://52.172.171.190:3001/core/tag/create"
        response=requests.post(url=tag_create,data=content)
        response=response.json()
        
        return {"success":"true","message":"tag registered successfully","data":response["data"]}
    except:
        raise InternalServerError

def edit_tag(content,params):
    try:
        #update
        tag_update="http://52.172.171.190:3001/core/tag/updateOne"
        query={"tag":params["tag_name"],"category":params["tag_category"]}

        response=requests.post(url=tag_update,params=query,data=content)
        response=response.json()
        
        return {"success":"true","message":"tag edited successfully","data":response["data"]}
    except:
        raise InternalServerError




def register_category(content):
    try:
        #check
        category_core_get="http://52.172.171.190:3001/core/category/get"
    
        check_category=requests.get(url=category_core_get,params={"category":content["category"]})
        check_category=check_category.json()
        if "data" in check_category:
            if check_category["data"] is None:
                pass
            else:
                return {"success":"false","message":"category already exists","data":{}}

        #create
        category_create="http://52.172.171.190:3001/core/category/create"
        response=requests.post(url=category_create,data=content)
        response=response.json()
        
        return {"success":"true","message":"category registered successfully","data":response["data"]}
    except:
        raise InternalServerError

def edit_category(content,params):
    try:
        #update
        category_upd="http://52.172.171.190:3001/core/category/updateOne"
        response=requests.post(url=category_upd,params={"category":params["category_name"]},data=content)
        response=response.json()
        
        return {"success":"true","message":"category edited successfully","data":response["data"]}
    except:
        raise InternalServerError

def delete_category(params):
    try:
        #delete
        category_del="http://52.172.171.190:3001/core/category/delete"
        response=requests.get(url=category_del,params={"category":params["category_name"]})
        response=response.json()
        
        return {"success":"true","message":"category deleted successfully","data":response["data"]}
    except:
        raise InternalServerError