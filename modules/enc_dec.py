from cryptography.fernet import Fernet


def encrypt_password(password):
    key = Fernet.generate_key()
    fernet = Fernet(key)
    enc_password=fernet.encrypt(password.encode()) 
    
    return {"encrypted_password":enc_password.decode('utf8').replace("'", '"'), "encryption_key":key.decode('utf8').replace("'", '"')}

def decrypt_password(encrypted_password,encryption_key):
    fernet = Fernet(encryption_key.encode('utf8'))
    dec_password=fernet.decrypt(encrypted_password.encode('utf8')).decode()
    return dec_password
    


