import requests
from api_access_layer.error_handlers.errors import InternalServerError
from datetime import datetime
from api_access_layer.modules.enc_dec import encrypt_password,decrypt_password
from flask_jwt_extended import create_access_token, create_refresh_token, jwt_required, get_jwt_identity
def create_login(content):
   
    #check
    url1="http://52.172.171.190:3001/core/publisher/get"
    query={
        "publisher":content["publisher"],
        "email":content["email"],
        "full_name":content["full_name"]
    }
    response1=requests.get(url=url1,params=query)
    response1=response1.json()
    if response1["data"] is None:
        pass
    else:
        return {"success":"false","message":"profile already exists","data":{"email":response1["data"]["email"]}} 

    
    url="http://52.172.171.190:3001/core/publisher/create"
    content["degrees"]=""
    content["status"]="Invited on "+str(datetime.now().date())
    content["current_emplyment"]=""
    content["admin"]="Yes"
    content["client"]="Yes"
    
    # password_data=encrypt_password(content["password"])
    # content.update(password_data)
    # content.pop("password",None)
    response=requests.post(url,json=content)
    response=response.json()
    # access_token = create_access_token(identity = content['email'])
    # refresh_token = create_refresh_token(identity = content['email'])
    data=content
    return {"success":"true","message":"created profile successfully","data":data}

    


def update_login(content):
    try:
        url="http://52.172.171.190:3001/core/publisher/updateOne"
        query={ 
                "publisher":content["publisher"],
                "full_name":content["full_name"],
                "email":content["email"]
                }
        if "password" in content:
            password_data=encrypt_password(content["password"])
           
            content.update(password_data)
            content.pop("password",None)
        else:
            pass

        response=requests.post(url=url,params=query,data=content)
        response=response.json()
        access_token = create_access_token(identity = content['email'])
        refresh_token = create_refresh_token(identity = content['email'])
        data={
            "email":content["email"],
            "access_token":access_token,
            "refresh_token":refresh_token
            }
        return {"success":"true","message":"created profile login credentials successfully","data":data}
    except Exception as e:
        raise InternalServerError

def validate_login(content):
    
    url="http://52.172.171.190:3001/core/publisher/get"
    response=requests.get(url,params={"email":content["email"]})
    response=response.json()
    if "data" in response:
        if response["data"] is None:
            return {"success":"fail","message":"invalid email","data":{}}  
        if "encrypted_password" in response["data"]:
            enc_password=response["data"]["encrypted_password"]
            enc_key=response["data"]["encryption_key"]
            password=decrypt_password(encrypted_password=enc_password,encryption_key=enc_key)
            if content["password"]==password:
                access_token = create_access_token(identity = content['email'])
                refresh_token = create_refresh_token(identity = content['email'])
                data={
                    "email":content["email"],
                    "access_token":access_token,
                    "refresh_token":refresh_token
                    }
                return {"success":"true","message":"login successful","data":data}
            else:
                return {"success":"fail","message":"invalid password","data":{}}
        else:
             return {"success":"fail","message":"invalid credentials","data":{}}  
    else:
        return {"success":"fail","message":"invalid email","data":{}}





