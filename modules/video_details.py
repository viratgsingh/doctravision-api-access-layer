import requests
from api_access_layer.error_handlers.errors import InternalServerError
from datetime import datetime
import random
from moviepy.video.io.ffmpeg_tools import ffmpeg_extract_subclip
import os
def extract_video(params):
    #check link
    video_core_get="http://52.172.171.190:3001/core/video/get"
    
    check_video=requests.get(url=video_core_get,params={"link":params["link"],"type":params["type"]})
    check_video=check_video.json()
    if "data" in check_video:
        if check_video["data"] is None:
            return {"success":"false","message":"invalid media link/type","data":{}}
        else:
            pass
    
    #extract media
    s3_link = requests.get(params["link"], allow_redirects=True)
    frmt=params["link"].split('/')[-1].split('.')[1]
    media_content=s3_link.content
    filename='trial_'+str(random.randint(10000000,99999999))+"."+frmt
    extract_name="extract_"+str(random.randint(10000000,99999999))+"."+frmt
    open(filename, 'wb').write(media_content)
    start_time=params["start"].split(":")
    start_hour=int(start_time[0]) * 3600
    start_minutes=int(start_time[1]) * 60
    start_seconds=int(start_time[2])
    start=start_hour+start_minutes+start_seconds
    
    end_time=params["end"].split(":")
    end_hour=int(end_time[0]) * 3600
    end_minutes=int(end_time[1]) * 60
    end_seconds=int(end_time[2])
    end=end_hour+end_minutes+end_seconds
    ffmpeg_extract_subclip(filename, start, end, targetname=extract_name)
    os.remove(filename)
    extract_content=open(extract_name, 'rb').read()
    os.remove(extract_name)

    #upload extract content
    
    input_folder="Extracts"
    video_name="extract_"+str(random.randint(10000000,99999999))+"."+frmt
    video_name=video_name.replace(" ","_")
    s3_layer="http://52.172.171.190:3501/vendor/file/uploadFile"
    data={
            "file":(video_name,extract_content),"folder":(None,input_folder)
    }
    extract_video=requests.post(url=s3_layer,files=data)
    extract_video= extract_video.json()
    extract_link=extract_video["data"]["Location"]


    video_core_create="http://52.172.171.190:3001/core/video/create"
    extract_data={
                "media_type":params["type"],
                "name":video_name,
                "type":"extract",
                "start":params["start"],
                "end":params["end"],
                "link":extract_link
                }
    save_extract=requests.post(url=video_core_create,data=extract_data)
    save_extract=save_extract.json()
    
    return {"success":"true","message":"extracted clip successfully","data":save_extract["data"]}
    
def get_extract_info(params,content):

    video_core_upd="http://52.172.171.190:3001/core/video/updateOne"
    
    try:
        get_extract_info=requests.post(url=video_core_upd,params={"link":params["link"],"media_type":params["media_type"]},data=content)
        get_extract_info=get_extract_info.json()
        return {"success":"true","message":"updated extract info successfully","data":get_extract_info["data"]}
    except Exception as e:
        InternalServerError

def get_download_link(params):
    try:
        video_core_get="http://52.172.171.190:3001/core/video/get"
        response=requests.get(url=video_core_get, params=params)
        response=response.json()
        link=response["data"]["link"]
        return {"success":"true","message":"fetched download link successfully","data":{"link":link}}
    except Exception as e:
        raise InternalServerError

