import requests
from datetime import datetime
from api_access_layer.error_handlers.errors import InternalServerError
def get_categories(email):
    url="http://52.172.171.190:3001/core/category/list"
    try:
        #get user's publisher
        response=requests.get(url="http://52.172.171.190:3001/core/publisher/get",params={"email":email})
        result=response.json()
        if result["data"] is None:
            publisher=""
        else:
            publisher=result["data"]["publisher"]
        response=requests.get(url=url,params={"publisher":publisher})
        result=response.json()
        data=result["data"]
        # category_l=[]
        # for category_info in data:
        #     try:
        #         category=category_info["category"]
        #         category_l.append(category)
        #     except:
        #         continue

        return {"success":"true","message":"fetched category data successfully","data":data}
        
    except Exception as e:
      raise InternalServerError

def get_subcategories(params,email):
    url="http://52.172.171.190:3001/core/category/list"
    try:
        #get user's publisher
        response=requests.get(url="http://52.172.171.190:3001/core/publisher/get",params={"email":email})
        result=response.json()
        if result["data"] is None:
            params["publisher"]=""
        else:
            params["publisher"]=result["data"]["publisher"]
        response=requests.get(url=url,params=params)
        result=response.json()
        try:
            subcategories=result["data"][0]["subcategories"]
        except:
            subcategories=result["data"]

        return {"success":"true","message":"fetched subcategory data successfully","data":subcategories}
        
    except Exception as e:
      raise InternalServerError

def get_subcategorytags(params,email):
    url="http://52.172.171.190:3001/core/sCategory/list"
    try:
        #get user's publisher
        response=requests.get(url="http://52.172.171.190:3001/core/publisher/get",params={"email":email})
        result=response.json()
        if result["data"] is None:
            params["publisher"]=""
        else:
            params["publisher"]=result["data"]["publisher"]
        response=requests.get(url=url,params=params)
        result=response.json()
        try:
            tags=result["data"][0]["tags"]
        except:
            tags=result["data"]

        return {"success":"true","message":"fetched subcategory tags data successfully","data":tags}
        
    except Exception as e:
      raise InternalServerError

def get_alltags(params,email):
    url="http://52.172.171.190:3001/core/tag/list"
    #get user's publisher
    response=requests.get(url="http://52.172.171.190:3001/core/publisher/get",params={"email":email})
    result=response.json()
    if result["data"] is None:
        params["publisher"]=""
    else:
        params["publisher"]=result["data"]["publisher"]

    if (params["classification"] is None) | (params["classification"]=="category"):
        

        categories=get_categories(email)
        category_l=categories["data"]
        data={"data":[{}]}
        
        for category in category_l:
            response=requests.get(url=url,params={"category":category["category"],"publisher":params["publisher"]})
            response=response.json()
            data["data"][0][category["category"]]=response["data"]

    elif params["classification"]=="trending":

        categories=get_categories(email)
        category_l=categories["data"]
        data={"data":[{}]}
        
        for category in category_l:
            response=requests.get(url=url,params={"category":category["category"],"trending":"Yes","publisher":params["publisher"]})
            response=response.json()
            data["data"][0][category["category"]]=response["data"]
        

    elif params["classification"]=="most_referred":
        categories=get_categories(email)
        category_l=categories["data"]
        data={"data":[{}]}

        for category in category_l:
            response=requests.get(url=url,params={"category":category["category"],"most_referred":"Yes","publisher":params["publisher"]})
            response=response.json()
            data["data"][0][category["category"]]=response["data"]

    elif params["classification"]=="recently_added":
        categories=get_categories(email)
        category_l=categories["data"]
        data={"data":[{}]}

        for category in category_l:
            response=requests.get(url=url,params={"category":category["category"],"publisher":params["publisher"]})
            response=response.json()
            data["data"][0][category["category"]]=response["data"][::-1]
    
    elif params["classification"]=="alphabetical":
        
        categories=get_categories(email)
        category_l=categories["data"]
        data={"data":[{}]}

        for category in category_l:
            response=requests.get(url=url,params={"category":category["category"],"publisher":params["publisher"]})
            result=response.json()
            result.pop("status",None)
            tags=result["data"]
            alp_tags={}
            for tag in tags:
                try:
                    text=tag["tag"].lower()
                    alp_tags[text]=tag
                except:
                    continue
            tag=list(alp_tags.items())
            tag=sorted(tag)
            data["data"][0][category["category"]]=[]
            for i in tag:
                data["data"][0][category["category"]].append(i[1])

    else:
        categories=get_categories(email)
        category_l=categories["data"]
        data={"data":[{}]}
        
        for category in category_l:
            response=requests.get(url=url,params={"category":category["category"],"publisher":params["publisher"]})
            response=response.json()
            data["data"][0][category["category"]]=response["data"]
    
        
    return {"success":"true","message":"fetched tags data successfully","data":data["data"]}
        
    
   

        