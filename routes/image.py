from api_access_layer.blueprints.image import CaptureFrame, CaptureFrame360,EditFrame
def image_routes(api):
    api.add_resource(CaptureFrame, '/api/access/publisher/image/capture')
    api.add_resource(CaptureFrame360, '/api/access/publisher/image360/capture')
    api.add_resource(EditFrame, '/api/access/publisher/image/edit')