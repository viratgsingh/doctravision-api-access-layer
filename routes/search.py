from api_access_layer.blueprints.searchmedmedia import FilterResults,Search, RequestMedia, SearchRecommendations
def search_routes(api):
    api.add_resource(Search, '/api/access/publisher/search')
    api.add_resource(SearchRecommendations, '/api/access/publisher/search/recommendations')
    api.add_resource(FilterResults, '/api/access/publisher/search/filter')
    api.add_resource(RequestMedia, '/api/access/publisher/search/request/media')