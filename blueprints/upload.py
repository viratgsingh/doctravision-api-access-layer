from flask import Blueprint,Flask, request
from flask_restful import Resource, Api, reqparse
import requests
from api_access_layer.modules.upload import upload_media, upload_media_info, upload_media_taginfo, upload_media_view
from api_access_layer.modules.register import register_tag, register_category,edit_category,edit_tag,delete_category

upload = Blueprint('upload', __name__)

class UploadVideo(Resource):
    def post(self):
        media_type="video"
        input_file=request.files.getlist("file")[0]

        result=upload_media(input_file=input_file,media_type=media_type)
        
        #result={"status_code":response.status_code}
        return result

class UploadVideoInfo(Resource):
    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument('category', type=str, required=True)
        parser.add_argument('subcategory', type=str, required=True)
        parser.add_argument('name', type=str, required=True)
        parser.add_argument('description', type=str, required=True)
        parser.add_argument('link', type=str, required=True)
        args=parser.parse_args()
        content=request.get_json()
        content["type"]="video"
        result=upload_media_info(content=content)
        return result

class UploadVideoTagInfo(Resource):
    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument('name', type=str, required=True)
        parser.add_argument('tag', type=str, required=True)
        parser.add_argument('description', type=str, required=False)
        parser.add_argument('start', type=str, required=True)
        parser.add_argument('end', type=str, required=True)

        
        args=parser.parse_args()
        content=request.get_json()
        content["type"]="video"
        result=upload_media_taginfo(content=content)
        return result

class UploadVideoView(Resource):
    def get(self):
        parser = reqparse.RequestParser()
        parser.add_argument('name', type=str, required=True)
        args=parser.parse_args()
        args["type"]="video"
        result=upload_media_view(params=args)
        return result

class UploadVideo360(Resource):
    def post(self):
        media_type="video360"
        input_file=request.files.getlist("file")[0]

        result=upload_media(input_file=input_file,media_type=media_type)
        
        #result={"status_code":response.status_code}
        return result

class UploadVideo360Info(Resource):
    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument('category', type=str, required=True)
        parser.add_argument('subcategory', type=str, required=True)
        parser.add_argument('name', type=str, required=True)
        parser.add_argument('description', type=str, required=True)
        parser.add_argument('link', type=str, required=True)
        args=parser.parse_args()
        content=request.get_json()
        content["type"]="video360"
        result=upload_media_info(content=content)
        return result

class UploadVideo360TagInfo(Resource):
    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument('name', type=str, required=True)
        parser.add_argument('tag', type=str, required=True)
        parser.add_argument('start', type=str, required=True)
        parser.add_argument('end', type=str, required=True)

        
        args=parser.parse_args()
        content=request.get_json()
        content["type"]="video360"
        result=upload_media_taginfo(content=content)
        return result

class UploadVideo360View(Resource):
    def get(self):
        parser = reqparse.RequestParser()
        parser.add_argument('name', type=str, required=True)
        args=parser.parse_args()
        args["type"]="video360"
        result=upload_media_view(params=args)
        return result

class UploadImage(Resource):
    def post(self):
        media_type="image"
        input_file=request.files.getlist("file")[0]

        result=upload_media(input_file=input_file,media_type=media_type)
        
        #result={"status_code":response.status_code}
        return result

class UploadImageInfo(Resource):
    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument('category', type=str, required=True)
        parser.add_argument('subcategory', type=str, required=True)
        parser.add_argument('name', type=str, required=True)
        parser.add_argument('description', type=str, required=True)
        parser.add_argument('link', type=str, required=True)
        args=parser.parse_args()
        content=request.get_json()
        content["type"]="image"
        result=upload_media_info(content=content)
        return result

class UploadImageTagInfo(Resource):
    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument('name', type=str, required=True)
        parser.add_argument('tag', type=str, required=True)

        
        args=parser.parse_args()
        content=request.get_json()
        content["type"]="image"
        content["start"]="00:00:00"
        content["end"]="00:00:00"
        result=upload_media_taginfo(content=content)
        return result

class UploadImageView(Resource):
    def get(self):
        parser = reqparse.RequestParser()
        parser.add_argument('name', type=str, required=True)
        args=parser.parse_args()
        args["type"]="image"
        result=upload_media_view(params=args)
        return result

class UploadImage360(Resource):
    def post(self):
        media_type="image360"
        input_file=request.files.getlist("file")[0]

        result=upload_media(input_file=input_file,media_type=media_type)
        
        #result={"status_code":response.status_code}
        return result

class UploadImage360Info(Resource):
    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument('category', type=str, required=True)
        parser.add_argument('subcategory', type=str, required=True)
        parser.add_argument('name', type=str, required=True)
        parser.add_argument('description', type=str, required=True)
        parser.add_argument('link', type=str, required=True)
        args=parser.parse_args()
        content=request.get_json()
        content["type"]="image360"
        result=upload_media_info(content=content)
        return result

class UploadImage360TagInfo(Resource):
    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument('name', type=str, required=True)
        parser.add_argument('tag', type=str, required=True)
        
        args=parser.parse_args()
        content=request.get_json()
        content["type"]="image360"
        content["start"]="00:00:00"
        content["end"]="00:00:00"
        result=upload_media_taginfo(content=content)
        return result

class UploadImage360View(Resource):
    def get(self):
        parser = reqparse.RequestParser()
        parser.add_argument('name', type=str, required=True)
        args=parser.parse_args()
        args["type"]="image360"
        result=upload_media_view(params=args)
        return result

class RegisterTag(Resource):
    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument('tag', type=str, required=True)
        parser.add_argument('category', type=str, required=True)
        parser.add_argument('description', type=str, required=True)
        args=parser.parse_args()
        content=request.get_json()
        result=register_tag(content=content)
        return result

class RegisterCategory(Resource):
    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument('category', type=str, required=True)
        parser.add_argument('description', type=str, required=True)
        args=parser.parse_args()
        content=request.get_json()
        result=register_category(content=content)
        return result

class EditTag(Resource):
    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument('tag_name', type=str, required=True)
        parser.add_argument('tag_category', type=str, required=True)
        args=parser.parse_args()
        content=request.get_json()
        result=edit_tag(content=content,params=args)
        return result

class EditCategory(Resource):
    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument('category_name', type=str, required=True)
        args=parser.parse_args()
        content=request.get_json()
        result=edit_category(content=content,params=args)
        return result

class DeleteCategory(Resource):
    def get(self):
        parser = reqparse.RequestParser()
        parser.add_argument('category_name', type=str, required=True)
        args=parser.parse_args()
        result=delete_category(params=args)
        return result