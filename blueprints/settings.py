from flask import Blueprint,Flask, request
from flask_restful import Resource, Api, reqparse
import requests
from api_access_layer.modules.settings import get_settings,get_edituser,get_inviteuser,edit_user,invite_user,get_notifications_settings,get_account_settings, invite_user_again

settings = Blueprint('settings', __name__)

class InviteUser(Resource):
    def get(self):
        parser = reqparse.RequestParser()
        parser.add_argument('full_name', type=str, required=True)
        parser.add_argument('email', type=str, required=True)
        parser.add_argument('degrees', type=str, required=False)
        parser.add_argument('current_employment', type=str, required=False)
        args=parser.parse_args()
        url=args["url"]
        args.pop("url",None)
    
        result=invite_user(content=args)
        
        return result

class EditUser(Resource):
    def get(self):
        parser = reqparse.RequestParser()
        parser.add_argument('full_name', type=str, required=True)
        parser.add_argument('email', type=str, required=True)
        parser.add_argument('degrees', type=str, required=False)
        parser.add_argument('current_employment', type=str, required=False)
        args=parser.parse_args()
        url=args["url"]
        args.pop("url",None)
    
        result=edit_user(content=args, request=request)
        
        return result

class GetSettings(Resource):
    def get(self):
    
        result=get_settings()
        
        return result

class GetEditUser(Resource):
    def get(self):
    
        result=get_edituser()
        
        #result={"status_code":response.status_code}
        return result

class GetInviteUser(Resource):
    def get(self):
    
        result=get_inviteuser()
        
        #result={"status_code":response.status_code}
        return result

class GetNotificationSettings(Resource):
    def get(self):
    
        result=get_notifications_settings()
        
        #result={"status_code":response.status_code}
        return result

class GetAccountSettings(Resource):
    def get(self):
    
        result=get_account_settings()
        
        #result={"status_code":response.status_code}
        return result


class InviteUserAgain(Resource):
    def get(self):
        parser = reqparse.RequestParser()
        parser.add_argument('url', type=str, required=True)
        parser.add_argument('full_name', type=str, required=True)
        parser.add_argument('email', type=str, required=True)
        parser.add_argument('degrees', type=str, required=False)
        parser.add_argument('current_employment', type=str, required=False)
        args=parser.parse_args()
        url=args["url"]
        args.pop("url",None)
    
        result=invite_user_again(url=url , content=args)
        
        #result={"status_code":response.status_code}
        return result

