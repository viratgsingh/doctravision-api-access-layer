from flask import Blueprint,Flask, request ,Response
from flask_restful import Resource, Api, reqparse
from flask_jwt_extended import create_access_token, create_refresh_token, jwt_required, get_jwt_identity
import requests
from api_access_layer.modules.login import create_login, validate_login ,update_login

logindetails = Blueprint('logindetails', __name__)



class CreateLogin(Resource):
    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument('full_name', type=str, required=True)
        parser.add_argument('email', type=str, required=True)
        parser.add_argument('password', type=str, required=True)
        args=parser.parse_args()
        content=request.get_json()
        result=update_login(content=content)
        
        return result

class ValidateLogin(Resource):
    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument('email', type=str, required=True)
        parser.add_argument('password', type=str, required=True)
        args=parser.parse_args()
        content=request.get_json()
        result=validate_login(content=content)
        
        return result

class AddNewClient(Resource):
    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument('publisher', type=str, required=True)
        parser.add_argument('full_name', type=str, required=True)
        parser.add_argument('email', type=str, required=True)
        parser.add_argument('contact', type=str, required=True)
        parser.add_argument('payment_mode', type=str, required=True)
        parser.add_argument('plan', type=str, required=True)
        args=parser.parse_args()
        content=request.get_json()
        
        result=create_login(content=content)
        
        return result

class TokenRefresh(Resource):
    @jwt_required(refresh=True)
    def post(self):
        current_user = get_jwt_identity()
        access_token = create_access_token(identity = current_user)
        return {"success":"true","message":"Successfully reissued access token","data":{'access_token': access_token}}
        
