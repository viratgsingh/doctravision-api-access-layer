import requests
from api_access_layer.error_handlers.errors import InternalServerError
from datetime import datetime
import random
from moviepy.editor import VideoFileClip
import os

def capture_frame(params):
    try:
        #check video
        video_core_get="http://52.172.171.190:3001/core/video/get"
        
        check_video=requests.get(url=video_core_get,params={"link":params["link"],"type":params["media_type"]})
        check_video=check_video.json()
        if "data" in check_video:
            if check_video["data"] is None:
                return {"success":"false","message":"invalid media name/type","data":{}}
            else:
                pass
        
        #get frame
        s3_link = requests.get(params["link"], allow_redirects=True)
        frmt=params["link"].split('/')[-1].split('.')[1]
        media_content=s3_link.content
        filename='trial_'+str(random.randint(10000000,99999999))+"."+frmt
        framename="frame_"+str(random.randint(10000000,99999999))+".png"
        open(filename, 'wb').write(media_content)
        start_time=params["timestamp"].split(":")
        start_hour=int(start_time[0]) * 3600
        start_minutes=int(start_time[1]) * 60
        start_seconds=int(start_time[2])
        start=start_hour+start_minutes+start_seconds
        clip = VideoFileClip(filename=filename)
        clip.save_frame(framename,start)
        clip.close()
        framecontent=open(framename, 'rb').read()
        os.remove(framename)
        os.remove(filename)

        #save frame
        input_folder="Frames"
        s3_layer="http://52.172.171.190:3501/vendor/file/uploadFile"
        data={
                "file":(framename,framecontent),"folder":(None,input_folder)
        }
        capture_frame=requests.post(url=s3_layer,files=data)
        capture_frame= capture_frame.json()
        frame_link=capture_frame["data"]["Location"]
        video_core_create="http://52.172.171.190:3001/core/video/create"
        frame_name=frame_link.split('/')[-1]
        save_frame=requests.post(url=video_core_create,data={"media_name":check_video["data"]["name"],"media_type":params["media_type"],"name":frame_name,"type":"frame","link":frame_link,"timestamp":params["timestamp"]})
        save_frame=save_frame.json()
        return {"success":"true","message":"captured frame successfully","data":save_frame["data"]}
    except Exception as e:
        raise InternalServerError

def edit_frame(input_file, frame):
    try:
        video_core_get="http://52.172.171.190:3001/core/video/get"
            
        check_frame=requests.get(url=video_core_get,params={"name":frame, "type":"frame"})
        check_frame=check_frame.json()
        if "data" in check_frame:
            if check_frame["data"] is None:
                return {"success":"false","message":"invalid frame name/type","data":{}}
            else:
                pass
        
        
        frmt=input_file.filename.split('.')[1]
        input_folder="Frames"
        image_name=check_frame["data"]["media_name"]+"_frame_"+str(random.randint(10000000,99999999))+"."+frmt
        image_name=image_name.replace(" ","_")
        s3_layer="http://52.172.171.190:3501/vendor/file/uploadFile"
        data={
                "file":(image_name,input_file),"folder":(None,input_folder)
        }
        capture_frame=requests.post(url=s3_layer,files=data)
        capture_frame= capture_frame.json()
        frame_link=capture_frame["data"]["Location"]
        frame_name=frame_link.split('/')[-1]


        video_core_upd="http://52.172.171.190:3001/core/video/updateOne"
        save_frame=requests.post(url=video_core_upd,params={"name":frame},data={"name":frame_name,"link":frame_link})
        save_frame=save_frame.json()
        return {"success":"true","message":"edited frame successfully","data":save_frame["data"]}
    except Exception as e:
        raise InternalServerError

    
    
