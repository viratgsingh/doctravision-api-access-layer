class InternalServerError(Exception):
    pass

class SchemaValidationError(Exception):
    pass


errors = {
    "InternalServerError": {
        "success":"false",
        "message": "something went wrong",
        "error_code":1000,
        "data": {}
    },
     "SchemaValidationError": {
        "success":"false",
        "message": "sequest is missing required parameters",
        "error_code":1002,
        "data": {}
     }
}