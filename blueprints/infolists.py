from flask import Blueprint,Flask, request
from flask_restful import Resource, Api, reqparse
from flask_jwt_extended import create_access_token, create_refresh_token, jwt_required, get_jwt_identity
import requests
from api_access_layer.modules.infolists import get_categories, get_subcategories, get_subcategorytags,get_alltags


infolists = Blueprint('infolists', __name__)

class GetCategoryList(Resource):
    @jwt_required()
    def get(self):
        try:
            email=get_jwt_identity()
        except:
            email=""
        result=get_categories(email=email)
        return result

class GetSubCategoryList(Resource):
    @jwt_required()
    def get(self):
        parser = reqparse.RequestParser()
        parser.add_argument('category', type=str, required=True)
        args=parser.parse_args()
        params=args
        email=get_jwt_identity()
        result=get_subcategories(params=args,email=email)
        
        return result

class GetTagList(Resource):
    @jwt_required()
    def get(self):
        parser = reqparse.RequestParser()
        parser.add_argument('subcategory', type=str, required=True)
        args=parser.parse_args()
        params=args
        email=get_jwt_identity()
        result=get_subcategorytags(params=params,email=email)
        
        return result

class GetAllTagsList(Resource):
    @jwt_required()
    def get(self):
        parser = reqparse.RequestParser()
        parser.add_argument('classification', type=str, required=False)
        args=parser.parse_args()
        params=args
        email=get_jwt_identity()
        result=get_alltags(params=params,email=email)
        
        return result

