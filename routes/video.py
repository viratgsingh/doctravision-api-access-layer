from api_access_layer.blueprints.video import ExtractVideo,ExtractVideo360,GetExtractInfo,GetExtractInfo360,GetDownload,GetDownload360

def video_routes(api):
    api.add_resource(ExtractVideo, '/api/access/publisher/video/extract')
    api.add_resource(ExtractVideo360, '/api/access/publisher/video360/extract')
    api.add_resource(GetExtractInfo, '/api/access/publisher/video/extract/info')
    api.add_resource(GetExtractInfo360, '/api/access/publisher/video360/extract/info')
    api.add_resource(GetDownload360,'/api/access/publisher/video360/download')
    api.add_resource(GetDownload,'/api/access/publisher/video/download')

    

