from flask import Blueprint,Flask, request
from flask_restful import Resource, Api, reqparse
import requests
from flask_jwt_extended import jwt_required, get_jwt_identity
from api_access_layer.modules.profile import  get_mediaadmin_info,get_mediaadmin_list,get_enduser_info,get_enduser_list,get_client_info,get_clientuser_list,editprofile,get_client_list,verifyemail, get_notifications, invite_user

profile = Blueprint('profile', __name__)

class EditProfilePage(Resource):
    @jwt_required()
    def post(self):
        
        params={"profile_email":get_jwt_identity()}
        content=request.get_json()
        result=editprofile(params=params, content=content)
        
        #result={"status_code":response.status_code}
        return result

class EditEndUser(Resource):
    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument('profile_email', type=str, required=True)
        
        args=parser.parse_args()
        content=request.get_json()
        result=editprofile(params=args, content=content)
        
        #result={"status_code":response.status_code}
        return result

class EditMediaAdmin(Resource):
    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument('profile_email', type=str, required=True)
        
        args=parser.parse_args()
        content=request.get_json()
        result=editprofile(params=args, content=content)
        
        #result={"status_code":response.status_code}
        return result

class InviteProfile(Resource):
    @jwt_required()
    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument('full_name', type=str, required=True)
        parser.add_argument('email', type=str, required=True)
        parser.add_argument('admin', type=str, required=True)
        parser.add_argument('degrees', type=str, required=False)
        parser.add_argument('current_employment', type=str, required=False)
        
        args=parser.parse_args()
        content=request.get_json()
        content["invited_by"]=get_jwt_identity()
        result=invite_user(content=content)
        
        #result={"status_code":response.status_code}
        return result

class VerifyProfilePage(Resource):
    @jwt_required()
    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument('verification_code', type=int, required=True)
        args=parser.parse_args()
        args["email"]=get_jwt_identity()
        result=verifyemail(content=args)
        
        return result

class GetClientList(Resource):
    def get(self):
    
        result=get_client_list()
        
        return result

class GetClientInfo(Resource):
    @jwt_required()
    def get(self):
        args={}
        args["email"]=get_jwt_identity()
        args["client"]="Yes"
        result=get_client_info(params=args)
        
        return result

class GetClientUserList(Resource):
    @jwt_required()
    def get(self):
        args={}
        args['invited_by']=get_jwt_identity()
        result=get_clientuser_list(params=args)
        
        return result

class GetEndUserInfo(Resource):
    def get(self):
        parser = reqparse.RequestParser()
        parser.add_argument('email', type=str, required=True)
        args=parser.parse_args()
        result=get_enduser_info(params=args)
        
        return result

class GetEndUserList(Resource):
    def get(self):
        result=get_enduser_list()
        
        return result

class GetMediaAdminInfo(Resource):
    def get(self):
        parser = reqparse.RequestParser()
        parser.add_argument('email', type=str, required=True)
        args=parser.parse_args()
        result=get_mediaadmin_info(params=args)
        
        return result

class GetMediaAdminList(Resource):
    def get(self):
        result=get_mediaadmin_list()
        
        return result

class GetNotifications(Resource):
    @jwt_required()
    def get(self):
        params={"email":get_jwt_identity()}
    
        result=get_notifications(params=params)
        
        return result

class GetProfilePage(Resource):
    def get(self):
        result=get_profilepage()
        return result

class GetProfileEditPage(Resource):
    def get(self):
        parser = reqparse.RequestParser()
        result=get_editprofile_page()
        return result

