from api_access_layer.blueprints.landing_page import LandingPage, LandingPageImage,LandingPageImage360,LandingPageVideo,LandingPageVideo360
def landing_page_routes(api):
    api.add_resource(LandingPageVideo, '/api/access/publisher/video')
    api.add_resource(LandingPageVideo360, '/api/access/publisher/video360')
    api.add_resource(LandingPageImage, '/api/access/publisher/image')
    api.add_resource(LandingPageImage360, '/api/access/publisher/image360')
    api.add_resource(LandingPage, '/api/access/publisher')
