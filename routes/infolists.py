from api_access_layer.blueprints.infolists import GetCategoryList, GetTagList, GetSubCategoryList, GetAllTagsList
def infolists_routes(api):
    api.add_resource(GetCategoryList, '/api/access/publisher/lists/categories')
    api.add_resource(GetSubCategoryList, '/api/access/publisher/lists/subcategories')
    api.add_resource(GetTagList, '/api/access/publisher/lists/tags')
    api.add_resource(GetAllTagsList, '/api/access/publisher/lists/tags/all')