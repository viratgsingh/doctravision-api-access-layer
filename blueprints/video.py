from flask import Blueprint,Flask, request
from flask_restful import Resource, Api, reqparse
import requests
from api_access_layer.modules.video_details import extract_video, get_extract_info, get_download_link

video = Blueprint('video', __name__)

class ExtractVideo(Resource):
    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument('start', type=str, required=True)
        parser.add_argument('end', type=str, required=True)
        parser.add_argument('link', type=str, required=True)
        args=parser.parse_args()
        args["type"]="video"
        result=extract_video(params=args)
        
        #result={"status_code":response.status_code}
        return result

class ExtractVideo360(Resource):
    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument('start', type=str, required=True)
        parser.add_argument('end', type=str, required=True)
        parser.add_argument('link', type=str, required=True)
        args=parser.parse_args()
        args["type"]="video360"
        result=extract_video(params=args)
        
        #result={"status_code":response.status_code}
        return result

class GetExtractInfo(Resource):
    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument('link', type=str, required=True)
        parser.add_argument('name', type=str, required=True)
        parser.add_argument('description', type=str, required=True)
        args=parser.parse_args()
        args["media_type"]="video"
        content=request.get_json()
        result=get_extract_info(params=args,content=content)
        
        #result={"status_code":response.status_code}
        return result

class GetExtractInfo360(Resource):
    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument('link', type=str, required=True)
        parser.add_argument('name', type=str, required=True)
        parser.add_argument('description', type=str, required=True)
        args=parser.parse_args()
        args["media_type"]="video360"
        content=request.get_json()
        result=get_extract_info(params=args,content=content)
        
        #result={"status_code":response.status_code}
        return result

class GetDownload(Resource):
    def get(self):
        parser = reqparse.RequestParser()
        parser.add_argument('name', type=str, required=True)
        args=parser.parse_args()
        args["media_type"]="video"
        args["type"]="extract"
        result=get_download_link(params=args)
        
        #result={"status_code":response.status_code}
        return result

class GetDownload360(Resource):
    def get(self):
        parser = reqparse.RequestParser()
        parser.add_argument('name', type=str, required=True)
        args=parser.parse_args()
        args["media_type"]="video360"
        args["type"]="extract"
        result=get_download_link(params=args)
        
        #result={"status_code":response.status_code}
        return result