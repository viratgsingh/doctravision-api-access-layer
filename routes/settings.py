from api_access_layer.blueprints.settings import GetAccountSettings,GetNotificationSettings,GetSettings,GetEditUser,GetInviteUser,InviteUser,EditUser,InviteUserAgain
def settings_routes(api):
    api.add_resource(GetSettings, '/api/access/settings')
    api.add_resource(GetAccountSettings, '/api/access/settings/account')
    api.add_resource(GetNotificationSettings, '/api/access/settings/notifications')
    api.add_resource(GetEditUser, '/api/access/settings/user/edit/page')
    api.add_resource(GetInviteUser, '/api/access/settings/user/invite/page')
    api.add_resource(InviteUser, '/api/access/settings/user/invite')
    api.add_resource(InviteUserAgain, '/api/access/settings/user/invite/again')
    api.add_resource(EditUser, '/api/access/settings/user/edit')